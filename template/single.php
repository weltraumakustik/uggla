<?php $template->get("header.php"); ?>
				<h2>
					<?php echo $article->title;?> 
					- <small>
						geschrieben am <?php date_default_timezone_set('America/New_York');  echo date('j.n.Y', $article->timestamp)?> von <?php echo $article->author;?>
					</small>
				</h2>
				<?php echo $article->content;?><br>
                Schlagworte: <?php echo $article->tags;?><br>
                <a href="index.php">&larr; Zurück</a>
<?php $template->get("footer.php"); ?>