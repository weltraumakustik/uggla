<!doctype html>
<html>
	<head>
		<meta charset="UTF-8" />

		<title>Dashboard - <?php echo SITE_NAME; ?></title>
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<!-- WICHTIG; SOLLTE SPÄTER WIE BEI WORDPRESS ÜDER EINE FUNCTION EINGEBUNDEN WERDEN -->
		<script type="text/javascript" src="../includes/lib/tinymce/jscripts/tiny_mce/tinymce.min.js"></script>
		<script type="text/javascript">
		tinyMCE.init({
			mode : "textareas",
			theme : "modern",        
            skin: 'light',
			language : "de",
			convert_urls : true,  
            menubar: "false",
            statusbar : false,
            theme_modern_default_foreground_color: "#000",
            browser_spellcheck : true,
            spellchecker_wordchar_pattern: /[^\s,\.]+/g,
            toolbar: "undo redo bold italic underline strikethrough removeformat image media link alignleft aligncenter alignright alignjustify  bullist numlist  formatselect print preview",
            plugins : 'advlist link image lists print preview code contextmenu media autoresize fullscreen',
		});
		</script>
        
        <script src='http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js'></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $("#profil-dropdown > .account").click(function(){
                    $("#profil-dropdown > .dropdown").fadeToggle("fast", function(){
                        if($(this).css('display') == "none")
                            $("#profil-dropdown > .account").removeClass("active");
                        else
                            $("#profil-dropdown > .account").addClass("active");
                    });
                });
            });
        </script>
        
		</head>

<body>
    <div id="ugtop">
        <div id="uglogo">
            <a href="index.php">
                <img src="img/ugdash/logo.png">
            </a>
        </div>
            
        <div id="ugright">
            <div id="ughelp">
                <a href="help.php">
                    Hilfe
                </a>  
            </div>
            <div id="ugsettings">
                <a href="settings.php">
                    Einstellungen
                </a>  
            </div>
            <?php  
            $email = "hallo@weltraumakustik.de";
            $default = "http://www.ugglacms.de/default_gravatar.jpg";
            $size = 31;
            $grav_url = "http://www.gravatar.com/avatar/" . md5( strtolower( trim( $email ) ) ) . "?d=" . urlencode( $default ) . "&s=" . $size; ?>
            <ul id="ugprofil_drop">
                <li>
                    <div id="profil-dropdown">
                        <div class="account">
                            <img src="<?php echo $grav_url; ?>" alt="Account"/>
                            <span>Howdy, <b><?php echo $_SESSION['user']['name']; ?>!</b></span>
                            <img src="img/ugdash/dash_profil/dropdown.png" alt="Dropdown"/>
                        </div>
                        <div class="dropdown" style="display: none">
                            <ul>
                                <li><a href="#"><img src="img/ugdash/dash_profil/upload.png" alt="Upload"/> Mein Profil</a></li>
                                <li><a href="logout.php"><img src="img/ugdash/dash_profil/log.png" alt="Upload"/> Abmelden</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul>       
        </div>
    </div>
    
<div id="ughead">
    <div id="innerhead">
        <ul>
            <?php
                if (strpos($_SERVER['PHP_SELF'], 'index.php'))
            { 
                    echo"<li class='ugactive'><a href='index.php'><img src='img/ugdash/ugdash.png'></a></li>";
            } else { 
                    echo"<li class='uglink'><a href='index.php'><img src='img/ugdash/ugdash.png'></a></li>";
            }
                if (strpos($_SERVER['PHP_SELF'], 'add.php'))
            { 
                    echo"<li class='ugactive'><a href='add.php'><img src='img/ugdash/ugbeitraege.png'></a></li>";
            } else { 
                    echo"<li class='uglink'><a href='add.php'><img src='img/ugdash/ugbeitraege.png'></a></li>";
            } ?>
            <li class="uglink"><a href="#"><img src="img/ugdash/ugkomm.png"></a></li>
            <li class="uglink"><a href="#"><img src="img/ugdash/ugseiten.png"></a></li>
            <li class="uglink"><a href="#"><img src="img/ugdash/ugeinst.png"></a></li>
        </ul>
    </div>
</div>

