<?php

require_once('../includes/config.php');
require_once('../includes/connection.php');
require_once('../includes/system.php');

$user = new User();

$articles = new Articles();

if ($user->logged_in()) {
	if(isset($_POST['submit'])){
		$error = $articles->create(trim($_POST['title']),trim($_POST['content']),trim($_POST['article_tags']));
	}
    if(!isset($error) && isset($_POST['title'], $_POST['content'], $_POST['article_tags'])) {
        echo "<span class='article-succeed fade-in'>Dein Artikel wurde gespeichert; Du wirst weitergeleitet!</span>";
            header( "Refresh:5; url=index.php", true, 303);
    }
 include('includes/header.php'); ?>
 </div>

<?php if (isset($error)) { ?>
    <span class="article-error fade-in">
        <?php echo $error; ?>
    </span>
<?php } ?>

<form action="add.php" method="post">
    <div id="article-title">
        <div class="article-title-inner">
            <input placeholder="Titel des Artikels" autocomplete="off" autofocus="true" type="text" id="article-title-input" required name="title">
        </div>
        <button type="submit" class="article-save-bttn" name="submit">Speichern</button>
    </div>
    <div id="article-text">
        <div class="article-text-inner">
            <textarea id="article-text-input" required name="content" rows="10" cols="50" style="height: 250px;">Hej, einfach schreiben...</textarea>
        </div>
    </div>
    <div id="article-tools">
        <div class="article-text-inner">
            <input placeholder="Schlagworte" autocomplete="off" type="text" id="article_tags" required name="article_tags">
        </div>
    </div>
</form>
      
		
<?php
	include('includes/footer.php');
	} else {
		header('Location: index.php');	
	}
?>
